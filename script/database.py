import mysql.connector
from mysql.connector import Error

try:
     connection = mysql.connector.connect(
        user='root',
        password='Krisko2606@',
        host='localhost'
        )
except Error as e:
    print("Error connecting to MySQL server", e)


def createBD():
    cursor = connection.cursor()
    try:
        cursor.execute("CREATE DATABASE poodle2")
        print("Database created")
    except Error as e:
        print("Database exists already")

    connectionv2 = mysql.connector.connect(
        user='root',
        password='Krisko2606@',
        host='localhost',
        database="poodle2"
    )
    cursor = connectionv2.cursor()
    try:
        cursor.execute("USE poodle2")
        with open('Create_DB.sql', 'r') as script_file:
            script = script_file.read()
            cursor.execute(script, multi=True)
    except Error as e:
        print("Error creating Relationships", e)


def add_data():
    connectionv3 = mysql.connector.connect(
        user='root',
        password='Krisko2606@',
        host='localhost',
        database="poodle2"

    )
    cursor = connectionv3.cursor()
    try:
        with open('add_data.sql', 'r') as data_file:
            statements = data_file.read().split(';')
            for statement in statements:
                if statement.strip():  # Skip empty statements
                    cursor.execute(statement)
            print("Data added")
            connectionv3.commit()
    except Error as e:
        print("Error adding data", e)


async def set_up_DB():
    await createBD()
    add_data()
    # print("Database created successfully")
    # print("Data Added")
