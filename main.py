from fastapi import FastAPI
# from app.routers.teachers import teachers_router
from app.auth.auth import auth_router
from app.routers.users import user_router
from app.routers.teachers import teachers_router
from app.routers.students import student_router
from app.routers.courses import course_router
from script.database import createBD, add_data
import uvicorn



app = FastAPI()
app.include_router(teachers_router)
app.include_router(user_router)
app.include_router(student_router)
app.include_router(auth_router)
app.include_router(course_router)

# @app.on_event("startup")
# async def startup_event():
#     createBD()
#     add_data()


if __name__ == "__main__":
    uvicorn.run("main:app", host="127.0.0.1", port=8000,
                reload=True, log_level="debug")
