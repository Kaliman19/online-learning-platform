from passlib.context import CryptContext

pass_crypt=CryptContext(schemes=["bcrypt"], deprecated="auto")

def password_hash(password:str):
    return pass_crypt.hash(password)

def verify(typed_pass,hashed_pass):
    return pass_crypt.verify(typed_pass,hashed_pass)
