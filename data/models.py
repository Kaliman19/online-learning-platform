from pydantic import BaseModel
from typing import Optional, List


class User(BaseModel):
    username: str
    password: str
    firstname: str
    lastname: str
    phone_number: Optional[str]
    linked_account: Optional[str]
    is_deactivated: Optional[bool] = False
    verified: Optional[bool] = False
    is_teacher: Optional[bool] = False
    is_admin: Optional[bool] = False
    premium_courses_left: Optional[int] = 5

    @classmethod
    def from_query_result(cls, username, password, firstname, lastname,
                          phone_number, linked_account, is_deactivated, verified, is_teacher, premium_courses_left):
        return cls(username=username, password=password,
                   firstname=firstname, lastname=lastname, phone_number=phone_number,
                   linked_account=linked_account,
                   is_deactivated=is_deactivated, verified=verified, is_teacher=is_teacher, premium_courses_left=premium_courses_left)


class DBuser(User):
    id: int

    @classmethod
    def create_dbuser(cls, id, username, password, firstname, lastname,
                      phone_number, linked_account, is_deactivated, verified, is_teacher, is_admin, premium_courses_left):
        return cls(id=id, username=username, password=password,
                   firstname=firstname, lastname=lastname, phone_number=phone_number,
                   linked_account=linked_account,
                   is_deactivated=is_deactivated, verified=verified, is_teacher=is_teacher, is_admin=is_admin, premium_courses_left=premium_courses_left)


class Teacher(BaseModel):
    id: int
    email: str
    password: str
    firstname: str
    lastname: str
    phone_num: str
    linked_acc: str

    @classmethod
    def from_query_result(cls, id, username, password, firstname, lastname, phone_num, linked_acc):
        return cls(id=id, username=username, password=password, firstname=firstname, lastname=lastname, phone_num=phone_num, linked_acc=linked_acc)


class TeacherAccInfoResponse(BaseModel):
    id: int
    username: str
    fname: str
    lname: str
    phone_num: str
    linked_acc: str

    @classmethod
    def from_query_result(cls, id, username, fname, lname, phone_num, linked_acc):
        return cls(id=id, username=username, fname=fname, lname=lname, phone_num=phone_num, linked_acc=linked_acc)


class TeacherAccInfoChange(BaseModel):
    fname: str
    lname: str
    phone_num: str
    linked_acc: str

    @classmethod
    def from_query_result(cls, id, fname, lname, phone_num, linked_acc):
        return cls(id=id, fname=fname, lname=lname, phone_num=phone_num, linked_acc=linked_acc)


class DBSection(BaseModel):
    id: int
    title: str
    course_id: int
    content: str

    @classmethod
    def from_query_result(cls, id, title, course_id, content):
        return cls(id=id, title=title, course_id=course_id, content=content)


class CreateSection(BaseModel):
    title: str
    course_title: str
    content: str

    @classmethod
    def from_query_result(cls, title, course_title, content):
        return cls(title=title, course_title=course_title, content=content)


class ViewSection(BaseModel):
    title: str
    course_id: int
    content: str

    @classmethod
    def from_query_result(cls, title, course_id, content):
        return cls(title=title, course_id=course_id, content=content)


class EditSection(BaseModel):
    title: str
    content: str


class StudentsAccInfoResponse(BaseModel):
    id: int
    username: str
    fname: str
    lname: str

    @classmethod
    def from_query_result(cls, id, username, fname, lname):
        return cls(id=id, username=username, fname=fname, lname=lname)


class StudentCoursesResponse(BaseModel):
    id: int
    course_title: str
    course_description: str
    course_objective: str
    teacher: str
    is_premium: bool
    total_sections: int

    @classmethod
    def from_query_result(cls, id, course_title, course_description, course_objective, teacher, is_premium, total_sections):
        return cls(id=id, course_title=course_title, course_description=course_description,
                   course_objective=course_objective, teacher=teacher, is_premium=is_premium, total_sections=total_sections)


class StudentsAccInfoChange(BaseModel):
    fname: str
    lname: str

    @classmethod
    def from_query_result(cls, fname, lname):
        return cls(fname=fname, lname=lname)


class Course(BaseModel):
    id: int
    title: str
    description: str
    objective: str
    owner: str
    is_premium: bool = False
    is_deactivated: bool = False
    sections: Optional[List[ViewSection]]
    home_page: str

    @classmethod
    def from_query_result(cls, id, title, description, objective, owner, is_premium, is_deactivated, sections, home_page):
        return cls(id=id, title=title, description=description, objective=objective,
                   owner=owner, is_premium=is_premium, is_deactivated=is_deactivated, sections=sections, home_page=home_page)


class StudentsCourseResponse(BaseModel):
    id: int
    title: str
    description: str
    objective: str
    is_premium: bool
    is_deactivated: bool
    sections: Optional[List[ViewSection]]
    home_page: str

    @classmethod
    def from_query_result(cls, id, title, description, objective,  is_premium, is_deactivated, sections, home_page):
        return cls(id=id, title=title, description=description, objective=objective,
                   is_premium=is_premium, is_deactivated=is_deactivated, sections=sections, home_page=home_page)


class CoursesAvailable(BaseModel):
    id: int
    title: str
    description: str
    objective: str
    owner: str
    is_premium: bool
    home_page: str

    @classmethod
    def from_query_result(cls, id, title, description, objective, owner, is_premium, home_page):

        return cls(id=id, title=title, description=description, objective=objective, owner=owner,
                   is_premium=is_premium,  home_page=home_page)


class CreateCourse(BaseModel):
    title: str
    description: str
    objective: str
    owner: Optional[str]
    is_premium: bool = False
    is_deactivated: bool = False
    home_page: str

    @classmethod
    def from_query_result(cls, title, description, objective, tags, owner, sections, is_premium=False, is_deactivated=False):
        return cls(title=title, description=description, objective=objective, tags=tags,
                   owner=owner, sections=sections, is_premium=is_premium, is_deactivated=is_deactivated)


class ViewCourse(BaseModel):
    title: str
    description: str
    objective: str
    owner: str
    is_premium: bool = False
    is_deactivated: bool = False
    home_page: str
    section: Optional[List[ViewSection]] = None

    @classmethod
    def from_query_result(cls, title, description, objective, owner, is_premium, is_deactivated, home_page, section):
        return cls(title=title, description=description,
                   objective=objective, owner=owner, is_premium=is_premium,
                   is_deactivated=is_deactivated, home_page=home_page, section=section)


class PublicCoursesInfo(BaseModel):
    title: str
    description: str
    tag_name: str

    @classmethod
    def from_query_result(cls, title, description, tag_name):
        return cls(title=title, description=description, tag_name=tag_name)


class ViewGenCourses(BaseModel):
    title: str
    description: str

    @classmethod
    def from_query_result(cls, title, description):
        return cls(title=title, description=description)

# ------------------------------------------------


class Student(BaseModel):
    id: int
    email: str
    password: str
    firstname: str
    lastname: str
    premium_courses_left: int = 5
    courses: list = []  # List[DBCourse]

    @classmethod
    def from_query_result(cls, id, email, password, firstname, last_name, premium_courses_left, courses=None):
        return cls(id=id, email=email, password=password, firstname=firstname,
                   last_name=last_name, premium_courses_left=premium_courses_left, courses=courses or None)


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenInfo(BaseModel):
    id: int
    username: str
    is_admin: bool
    is_teacher: bool


class UpdateCourse(BaseModel):
    title: str
    description: str
    objective: str
    is_premium: bool
    is_deactivated: bool
    home_page: str

    @classmethod
    def from_query_result(cls, title, description, objective, owner, is_premium, is_deactivated, home_page):
        return cls(title=title, description=description, objective=objective, owner=owner, is_premium=is_premium, is_deactivated=is_deactivated, home_page=home_page)


class StudentViewCourse(BaseModel):
    id: int
    title: str
    description: str
    objective: str
    home_page: str
    section: List[DBSection]

    @classmethod
    def from_query_result(cls, id, title, description, objective, home_page, section):
        return cls(id=id, title=title, description=description,
                   objective=objective, home_page=home_page, section=section)


class StudentPostReview(BaseModel):
    rating: int
    description: Optional[str] = None

    @classmethod
    def from_query_result(cls, rating, description):
        return cls(rating=rating, description=description,)


class DBReview(BaseModel):
    id: int
    user_id: int
    courses_id: int
    rating: int
    description: Optional[str] = None

    @classmethod
    def from_query_result(cls, id, user_id, courses_id, rating, description):
        return cls(id=id, user_id=user_id, courses_id=courses_id, rating=rating, description=description,)
