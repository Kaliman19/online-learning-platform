from fastapi import APIRouter, Depends, HTTPException, Response
from app.services import student_services, auth_services, courses_services, teachers_services
from data.models import StudentPostReview, TokenInfo, StudentsAccInfoChange, Course, Student, StudentCoursesResponse
from app.services.auth_services import get_current_user
from typing import Optional

student_router = APIRouter(prefix='/students')


@student_router.get('/info')
def student_account(logged_user: TokenInfo = Depends(auth_services.get_current_user)):
    if logged_user and not logged_user.is_teacher:
        id = logged_user.id
    if logged_user.is_teacher:
        raise HTTPException(
            status_code=401, detail="You must be a student to view this info")
    result = student_services.view_acc_info(id)
    if not result:
        raise HTTPException(status_code=404, detail="No info found")
    return result


@student_router.put('/update')
def update_student_acc(new_info: StudentsAccInfoChange, logged_user: TokenInfo = Depends(auth_services.get_current_user)):
    if not logged_user or logged_user.is_teacher:
        raise HTTPException(status_code=401)
    id = logged_user.id
    current_details = student_services.view_acc_info(id)
    if not current_details:
        raise HTTPException(status_code=403)
    student_services.update_acc(id, new_info)
    return student_services.view_acc_info(id)


@student_router.get('/courses')
def get_course(logged_user: TokenInfo = Depends(auth_services.get_current_user)):
    if not logged_user:
        raise HTTPException(
            status_code=401, detail='You must be logged in to view your courses!')
    id = logged_user.id
    # if a course does not have sections it won't show up
    response = student_services.view_courses(id)
    if not response:
        raise HTTPException(status_code=404, detail="Course not found")

    return response


@student_router.get('/courses/available')
def get_course(name: Optional[str] = None, tag: Optional[str] = None, logged_user: TokenInfo = Depends(get_current_user)):

    if not logged_user:
        raise HTTPException(status_code=401, detail="You are not a student")
    result = student_services.all_courses(logged_user.username, name, tag)
    if not result:
        raise HTTPException(status_code=404, detail="No courses avaiable")
    return result


@student_router.post('/course/{course_id}/review')
def post_review(course_id: int, review: StudentPostReview, logged_user: TokenInfo = Depends(auth_services.get_current_user)):
    if not logged_user or logged_user.is_teacher:
        raise HTTPException(status_code=401, detail='You must be a student')
    if not student_services.check_if_enrolled(course_id, logged_user.id):
        raise HTTPException(
            status_code=401, detail='You are not enrolled in this course')
    if review.rating > 5 or review.rating <= 0:
        raise HTTPException(
            status_code=403, detail='Rate must be between 1 and 5')
    review = student_services.add_review_to_DB(
        course_id, review, logged_user.id)
    if not review:
        raise HTTPException(
            status_code=403, detail='You have already rated this course')
    return review


@student_router.delete('/subscription/{id}')
def unsubscribe(id: int, logged_user: TokenInfo = Depends(get_current_user)):
    if not logged_user or logged_user.is_teacher:
        raise HTTPException(status_code=401, detail="You are not a student")

    result = courses_services.unsubscribe_from_course(id, logged_user.id)
    if result:
        return Response(status_code=200, content="Successfully unsubscribed!")
    else:
        raise HTTPException(
            status_code=404, detail=f'You are not subscribed to course with id: {id}')


@student_router.post('/subscription/{id}')
def subscribe(id: int, logged_user: TokenInfo = Depends(get_current_user)):
    if not logged_user or logged_user.is_teacher:
        raise HTTPException(status_code=401, detail="You are not a student")
    check_premuim_courses_left = courses_services.check_premium_course_avaiability(
        id, logged_user.id)
    if not check_premuim_courses_left:
        raise HTTPException(
            status_code=403, detail='You have reached you maximum number of premium courses')
    result = courses_services.subscribe_to_course(id, logged_user.id)
    if result:
        return Response(status_code=200, content="Successfully enlisted! You will be able the view the course details after the teacher has approved your enrollment")
    else:
        raise HTTPException(
            status_code=401, detail="You already enrolled for this course")
