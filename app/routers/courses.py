from fastapi import APIRouter, Depends, HTTPException, Response
from app.services import courses_services, teachers_services, auth_services, student_services
from data.models import CreateCourse, Course, CreateSection, EditSection, TokenInfo, UpdateCourse
from app.services.auth_services import get_current_user
from typing import Optional

course_router = APIRouter(prefix='/courses')


@course_router.get("/")
# logged_user: TokenInfo = Dependget_current_user)):
def get_courses(name: Optional[str] = None, tag: Optional[str] = None,):
    courses = courses_services.get_public_courses(name, tag)
    return courses


@course_router.get('/{course_id}')
def get_course(course_id: int, logged_user: TokenInfo = Depends(auth_services.get_current_user)):
    if not logged_user:
        raise HTTPException(
            status_code=401, detail="Must be logged in to view this course")
    response = teachers_services.view_course(course_id)
    if logged_user.is_teacher:
        if not response:
            raise HTTPException(status_code=404, detail="Course not found")
        if not logged_user.is_teacher or response.owner != logged_user.username:
            raise HTTPException(
                status_code=401, detail="You are not authorised to view this course")
        return response
    else:
        response = student_services.get_course_by_id(
            course_id, logged_user.username)
    if not response:
        raise HTTPException(
            status_code=401, detail='You are not authorized to view this course')
    return response


@course_router.get('/{course_id}/section')
def get_all_section_from_course(course_id: int, logged_user: TokenInfo = Depends(auth_services.get_current_user)):
    # if not logged_user.is_teacher:
    #     raise HTTPException(status_code=401)
    if not logged_user:
        raise HTTPException(
            status_code=401, detail='You muse be logged in to view sections')
    # course = teachers_services.view_course(course_id)
    # if course.owner != logged_user.username:
    #     raise HTTPException(status_code=401)
    sections = teachers_services.get_all_section(course_id)
    if not sections:
        raise HTTPException(
            status_code=404, detail=f'Cannot find sections for course with id: {course_id}')
    return sections


@course_router.get('/{course_id}/section/{section_id}')
def get_course_section(course_id: int, section_id: int, logged_user: TokenInfo = Depends(auth_services.get_current_user)):
    if not logged_user:
        raise HTTPException(status_code=401)
    sections = teachers_services.get_section_by_id(course_id, section_id)
    if not sections:
        raise HTTPException(
            status_code=404, detail='Invalid course or section id')
    return sections


@course_router.post('/')
def create_course(course: CreateCourse, logged_user: TokenInfo = Depends(auth_services.get_current_user)):
    if not logged_user.is_teacher:
        raise HTTPException(status_code=401)
    result = teachers_services.create_course(course, logged_user.username)
    if not result:
        raise HTTPException(status_code=403, detail="Invalid course info")
    return teachers_services.view_course(result)


@course_router.post('/{course_id}/section')
def add_sections_to_course(course_id: int, section: CreateSection, logged_user: TokenInfo = Depends(auth_services.get_current_user)):
    if not logged_user.is_teacher:
        raise HTTPException(status_code=401)
    course = teachers_services.view_course(course_id)
    if not course:
        raise HTTPException(status_code=403, detail="Invalid Course id")
    if course.owner != logged_user.username:
        raise HTTPException(
            status_code=401, detail='You must own the course to make changes')
    result = teachers_services.add_section_to_course(section, course.id)
    if not result:
        return
    return result


@course_router.delete('/{course_id}/section/{section_id}')
def delete_section(course_id: int, section_id: int, logged_user: TokenInfo = Depends(auth_services.get_current_user)):
    if not logged_user.is_teacher:
        raise HTTPException(status_code=401)
    course = teachers_services.view_course(course_id)
    if course.owner != logged_user.username:
        raise HTTPException(
            status_code=401, detail='You must own the course to remove a section')
    result = teachers_services.remove_section_by_id(course_id, section_id)
    if not result:
        raise HTTPException(
            status_code=404, detail='Invalid course or section id')
    return Response(status_code=200, content=f'Successfully removed section with id: {section_id} from course with id: {course_id}')


@course_router.patch('/{course_id}')
def update_course(course_id: int, course: UpdateCourse, logged_user: TokenInfo = Depends(auth_services.get_current_user)):
    if not logged_user.is_teacher:
        raise HTTPException(status_code=401)
    course_exist = teachers_services.view_course(course_id)
    if not course_exist:
        raise HTTPException(status_code=404, detail="Invalid Course id")
    if course_exist.owner != logged_user.username:
        raise HTTPException(
            status_code=401, detail="You must be the owner of the course")
    unique_name = teachers_services.update_course(course_id, course)
    if not unique_name:
        raise HTTPException(
            status_code=409, detail="This title already exists")
    return teachers_services.view_course(course_id)


@course_router.put('/{course_id}/section/{section_id}')
def edit_section(course_id: int, section_id: int, section: EditSection, logged_user: TokenInfo = Depends(auth_services.get_current_user)):
    if not logged_user.is_teacher:
        raise HTTPException(status_code=401)
    course = teachers_services.view_course(course_id)
    if not course:
        raise HTTPException(status_code=404, detail="Invalid Course id")
    if course.owner != logged_user.username:
        raise HTTPException(
            status_code=401, detail="You must be the owner of the course")
    section_check = teachers_services.get_section_by_id(course_id, section_id)
    if not section_check:
        raise HTTPException(status_code=404, detail="Invalid Section id")
    new_section = teachers_services.edit_section_by_id(
        course_id, section_id, section)
    return new_section
