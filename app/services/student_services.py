from data.data import read_query, update_query, query_count, insert_query
from data.models import DBReview, DBSection, StudentPostReview, StudentViewCourse, StudentsAccInfoResponse, StudentsAccInfoChange, Course, StudentCoursesResponse, StudentsCourseResponse, CoursesAvailable
from app.services import teachers_services


def view_acc_info(id: int):
    '''Return a tuple with Student Account information'''
    data = read_query(
        "SELECT id,username,firstname,lastname FROM user WHERE id = ?", (id,))
    if not data:
        return
    return next((StudentsAccInfoResponse.from_query_result(*row) for row in data), None)


def update_acc(id: int, new_info: StudentsAccInfoChange):
    sql_sript = '''UPDATE user SET firstname=?,lastname=?  WHERE id=?'''
    new_info = (new_info.fname, new_info.lname, id)
    result = update_query(sql_sript, new_info)
    return result


def view_courses(id: int):
    '''Returns student's courses.'''
    # doesn't show courses which do not have sections
    data = read_query('''select c.id,c.title,c.description,c.objective,c.owner,c.is_premium,
                        (SELECT count(*) from poodle2.section as s where s.courses_id=c.id) as num_sections
                        from poodle2.user u 
                        JOIN poodle2.user_has_courses t ON t.user_id=u.id
                        JOIN poodle2.courses c on t.courses_id= c.id 
                        JOIN poodle2.section s on s.courses_id=c.id 
                        WHERE u.id=? and s.courses_id=c.id  and t.approved=1
                        GROUP BY c.id;''', (id,))

    return (StudentCoursesResponse.from_query_result(*row) for row in data)


def all_courses(username: str, name: str = None, tag: str = None):
    '''Returns a list with all courses which are not deactivated.
        Could be searched by name and tag'''
    if not name and not tag:
        all_courses_db = read_query(
            """SELECT c.id, c.title, c.description, c.objective, c.owner, c.is_premium, c.home_page
                FROM courses AS c
                WHERE c.is_deactivated = false AND c.id NOT IN (
                    SELECT courses_id FROM user_has_courses WHERE user_id IN (
                        SELECT u.id FROM user AS u WHERE u.username = ?
    ))""",(username,))
        response =(CoursesAvailable.from_query_result(*row) for row in all_courses_db)

    elif tag != None and name == None:
        all_courses_db = read_query(
            """SELECT c.id, c.title, c.description, c.objective, c.owner, c.is_premium, c.home_page
                FROM courses AS c
                INNER JOIN tags_has_courses as thc on thc.courses_id=c.id
                INNER JOIN tags as t on t.id=thc.tags_id 
                WHERE c.is_deactivated = false AND c.id NOT IN (
                    SELECT courses_id FROM user_has_courses WHERE user_id IN (
                        SELECT u.id FROM user AS u WHERE u.username = ?)) AND  t.tag_name=?
    """, (username,tag))
        response = (CoursesAvailable.from_query_result(*row) for row in all_courses_db)
    elif name != None and tag == None:
        all_courses_db = read_query(
            """SELECT c.id, c.title, c.description, c.objective, c.owner, c.is_premium, c.home_page
                FROM courses AS c
                INNER JOIN tags_has_courses as thc on thc.courses_id=c.id
                INNER JOIN tags as t on t.id=thc.tags_id 
                WHERE c.is_deactivated = false AND c.id NOT IN (
                    SELECT courses_id FROM user_has_courses WHERE user_id IN (
                        SELECT u.id FROM user AS u WHERE u.username = ?)) AND  c.title=?
    """, (username, name))
        response = (CoursesAvailable.from_query_result(*row) for row in all_courses_db)
    elif name and tag:
        all_courses_db = read_query(
            """SELECT c.id, c.title, c.description, c.objective, c.owner, c.is_premium, c.home_page
                FROM courses AS c
                INNER JOIN tags_has_courses as thc on thc.courses_id=c.id
                INNER JOIN tags as t on t.id=thc.tags_id 
                WHERE c.is_deactivated = false AND c.id NOT IN (
                    SELECT courses_id FROM user_has_courses WHERE user_id IN (
                        SELECT u.id FROM user AS u WHERE u.username = ?)) AND  c.title=? AND t.tag_name=?
    """, (username, name, tag))
        response = (CoursesAvailable.from_query_result(*row) for row in all_courses_db)

    return response


def unsubscribe_from_course(course_id: int, id: int):
    data = update_query(
        'DELETE FROM user_has_courses WHERE courses_id=? and user_id=?', (course_id, id))
    return data


def get_id_by_username(username: str):
    data = read_query(
        '''SELECT id from poodle2.user WHERE username=?''', (username,))
    return data[0][0]


def get_course_by_id(course_id: int, username: str):
    '''Returns a information about a course the students is enrolled in'''
    user_id = get_id_by_username(username)
    course_info_db = read_query(
        '''SELECT id,title,description,objective,home_page from poodle2.courses where id=?''', (course_id,))
    sections_info_db = read_query(
        '''SELECT * FROM poodle2.section where courses_id=?''', (course_id,))
    check_student_enrolled = read_query(
        '''SELECT approved FROM poodle2.user_has_courses WHERE user_id=? and courses_id=?''', (user_id, course_id))
    if not check_student_enrolled:
        return
    sections = [DBSection.from_query_result(*row) for row in sections_info_db]
    course = [StudentViewCourse.from_query_result(
        *row, sections) for row in course_info_db]
    return course


def check_if_enrolled(course_id: int, student_id: int) -> bool:
    '''Return True if student is enrolled and approved in course'''
    data = read_query(
        '''SELECT * FROM poodle2.user_has_courses WHERE courses_id=? and user_id=? and approved=1''', (course_id, student_id))
    if data:
        return True
    return False


def add_review_to_DB(course_id: int, review: StudentPostReview, user_id: int):
    '''Add a review to DB'''
    if _review_exists_already(course_id, user_id):
        return
    add_to_db = insert_query('''INSERT INTO poodle2.review (user_id,courses_id,rating,description) VALUES(?,?,?,?)''',
                             (user_id, course_id, review.rating, review.description))
    review_db = read_query(
        '''SELECT * from poodle2.review WHERE user_id=? and courses_id=?''', (user_id, course_id))
    return DBReview.from_query_result(*review_db[0])


def _review_exists_already(course_id: int, user_id: int) -> bool:
    review_db = read_query(
        '''SELECT * from poodle2.review WHERE user_id=? and courses_id=?''', (user_id, course_id))
    if review_db:
        return True
    return False
