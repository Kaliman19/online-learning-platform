from jose import JWSError, jwt
from datetime import datetime, timedelta
from fastapi import Depends, status, HTTPException
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from data.data import read_query
from data import utilities
from data.models import Token, TokenInfo, DBuser


oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login")

SECRET_KEY = "09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 800  # [minutes]


def get_token(info: dict):

    to_encode = info.copy()

    expire = datetime.utcnow() + timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)

    to_encode.update({"exp": expire})

    return jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)


def validate_token(token: str, login_error):
    try:

        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])

        id: int = payload.get("id")
        username: str = payload.get("username")
        is_admin: bool = payload.get("is_admin")
        is_teacher: bool = payload.get("is_teacher")

        if not username:
            raise login_error
        token_info = TokenInfo(id=id, username=username,
                               is_admin=is_admin, is_teacher=is_teacher)
    except JWSError:
        raise login_error

    return token_info


def get_current_user(token: str = Depends(oauth2_scheme),) -> TokenInfo:
    login_error = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Unable to validate login details",
        headers={"WWW-Authenticate": "Bearer"},
    )
    return validate_token(token, login_error)


def create_token(login_details: OAuth2PasswordRequestForm) -> Token:
    row = read_query(
        """SELECT id,username,password,firstname,lastname,phone_number,linked_account,
        is_deactivated,verified,is_teacher,is_admin,premium_courses_left FROM user
         WHERE user.username=?""", (login_details.username,))

    db_user = (DBuser.create_dbuser(*row[0]) if row else None)

    if not db_user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Student not found in DB")

    if not utilities.verify(login_details.password, db_user.password):
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN,
                            detail="Log-in details not valid")

    the_token = get_token(
        {"id": db_user.id, "username": db_user.username,
            "is_admin": db_user.is_admin, "is_teacher": db_user.is_teacher}
    )
    return Token(access_token=the_token, token_type="bearer")
