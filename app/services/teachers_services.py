from data.data import read_query, insert_query, update_query, query_count
from data.models import Course, CreateCourse, CreateSection, DBSection, EditSection, Teacher, TeacherAccInfoChange,\
    TeacherAccInfoResponse, UpdateCourse, ViewCourse, ViewSection
from app.services import student_services


def view_acc_info(id: int) -> TeacherAccInfoResponse:
    '''Return information about the Teachers account'''
    info = read_query(
        '''SELECT id,username,firstname,lastname,phone_number,linked_account FROM user WHERE id=?''', (id,))
    if not info:
        return
    result = TeacherAccInfoResponse.from_query_result(*info[0])
    return result


def update_acc(id: int, new_info: TeacherAccInfoChange):
    '''Updates DB with the new account information'''
    sql_sript = '''UPDATE user SET firstname=?,lastname=?,phone_number=?,linked_account=? WHERE id=?'''
    new_info = (new_info.fname, new_info.lname,
                new_info.phone_num, new_info.linked_acc, id)
    result = update_query(sql_sript, new_info)
    return result


def create_course(course: CreateCourse, username: str):
    '''Creates course and add its to the DB and returns the new course ID'''
    sql_sript = '''INSERT INTO courses(title,description,objective,owner,is_premium,is_deactivated,home_page) 
                    VALUES(?,?,?,?,?,?,?) '''
    values = (course.title, course.description, course.objective, username,
              course.is_premium, course.is_deactivated, course.home_page)
    title_exists = read_query(
        '''SELECT * from courses WHERE title=?''', (course.title,))
    if title_exists:
        return
    new_id = insert_query(sql_sript, values)
    if not new_id:
        return
    return new_id


def view_course(id: int) -> Course:
    '''Finds the course by ID and Returns a Course class instance '''
    sql_scrpt = '''SELECT * FROM courses WHERE id=?'''
    info = read_query(sql_scrpt, (id,))
    if not info:
        return
    id, title, description, objective, owner, is_premium, is_deactivated, home_page = info[0]
    get_sections = read_query(
        '''SELECT * FROM section WHERE courses_id=?''', (id,))
    sections = find_sections(id)
    result = Course.from_query_result(
        id, title, description, objective, owner, is_premium, is_deactivated, sections, home_page)
    return result


# def find_course_by_title(title: str):
#     info = read_query('''SELECT * FROM courses WHERE title=?''', (title,))
#     if not info:
#         return
#     id, title, description, objective, owner, is_premium, is_deactivated, home_page = info[0]
#     sections = find_sections(id)
#     result = Course.from_query_result(
#         id, title, description, objective, owner, is_premium, is_deactivated, sections, home_page)
#     return resultt


def find_sections(course_id: int):
    '''Find sections for the course and returns them in a list'''
    get_sections = read_query(
        '''SELECT * FROM section WHERE courses_id=?''', (course_id,))
    all_sections = []
    if len(get_sections) != 0:
        for section in get_sections:
            all_sections.append(ViewSection.from_query_result(
                section[1], section[2], section[3]))
    return all_sections


def add_section_to_course(section: CreateSection, course_id: int):
    '''Returns Course class instance with attached sections'''
    course = view_course(course_id)
    if not course:
        return
    course_section = ViewSection.from_query_result(
        section.title, course_id, section.content)
    course.sections.append(course_section)
    add_section_to_DB(course_section)
    return course


def add_section_to_DB(section: ViewSection):
    '''Add sections to DB'''
    section_id = insert_query('''INSERT INTO section(title,courses_id,content) VALUES(?,?,?)''',
                              (section.title, section.course_id, section.content))
    return section_id


def update_course(course_id: int, course: UpdateCourse):
    try:
        rowcount = update_query('''UPDATE courses SET title=?,description=?,objective=?,is_premium=?,
                                is_deactivated=?,home_page=? WHERE id=?''',
                                (course.title, course.description, course.objective, course.is_premium,
                                 course.is_deactivated, course.home_page, course_id))
        return True
    except:
        return False


def get_section_by_id(course_id: int, section_id: int) -> DBSection:
    '''Returns a lsit of DBSection class instance'''
    section = read_query(
        '''SELECT * FROM poodle2.section WHERE courses_id=? and id=?''', (course_id, section_id))
    if not section:
        return
    return DBSection.from_query_result(*section[0])  # for row in section


def get_all_section(course_id: int) -> DBSection:
    '''Returns a list with all sections for a specific course'''
    section = read_query(
        '''SELECT * FROM poodle2.section WHERE courses_id=?''', (course_id,))
    if not section:
        return
    return [DBSection.from_query_result(*row) for row in section]


def remove_section_by_id(course_id: int, section_id: int):
    '''Removes a section from a course and DB'''
    result = update_query(
        '''DELETE FROM poodle2.section WHERE courses_id=? and id=?''', (course_id, section_id))
    return result


def edit_section_by_id(course_id: int, section_id: int, section: EditSection) -> DBSection:
    new_section = get_section_by_id(course_id, section_id)
    if not new_section:
        return None
    new_section.title = section.title
    new_section.content = section.content
    return new_section


def get_enrolled_students(course_id: int):
    '''Returns a list with all students who are approved for a course'''
    all_student_ids = read_query(
        '''SELECT user_id FROM poodle2.user_has_courses WHERE courses_id=? and approved=0''', (course_id,))
    if not all_student_ids:
        return
    enrolled_students = []
    for student in all_student_ids:
        enrolled_students.append(student_services.view_acc_info(student[0]))
    return enrolled_students


def approve_students(course: Course, student_id: int) -> bool:
    '''Updates DB and enrolls a student in a course, if the course is premium it makes additional changes to DB'''
    if course.is_premium:
        _subscribe_to_premium_course(student_id)
    result = update_query(
        '''UPDATE poodle2.user_has_courses SET approved=1 WHERE courses_id=? and user_id=?''', (course.id, student_id))
    return result


def _subscribe_to_premium_course(student_id: int):
    '''Updates how many premium courses the student can enroll in'''
    premium_courses_left_db = read_query(
        '''SELECT premium_courses_left FROM user WHERE id=?''', (student_id,))
    premium_courses_left = premium_courses_left_db[0][0]-1
    check = update_query(
        '''UPDATE poodle2.user SET premium_courses_left=? WHERE id=?''', (premium_courses_left, student_id))
    return check
