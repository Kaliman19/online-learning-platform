from typing import Union, List
# from fastapi import status, HTTPException  # , Depends


from data import data
from data.models import User, DBuser, TokenInfo


def create_user(user: User, insert_func=None, get_row_func=None) -> Union[DBuser, None]:
    '''Creates a user and inserts in DB. Returns the created user'''

    vals = [v for v in user.dict().values()]

    # dependency injection:
    username = user.username
    password = user.password
    firstname = user.firstname
    lastname = user.lastname
    phone_number = user.phone_number
    linked_account = user.linked_account
    is_deactivated = user.is_deactivated
    verified = user.verified
    is_teacher = user.is_teacher
    is_admin = user.is_admin

    if insert_func is None:
        insert_user_sql = """INSERT INTO user (username, password, firstname, 
                         lastname, phone_number, linked_account,is_deactivated,
                         verified,is_teacher,is_admin)
                                 VALUES (?, ?, ?, ?, ?, ?, ?, ? ,? ,?);"""
        check_username = data.read_query(
            '''SELECT * from user WHERE username=?''', (username,))
        if check_username:
            return None
        id = data.insert_query(insert_user_sql, (username, password, firstname, lastname,
                               phone_number, linked_account, is_deactivated, verified, is_teacher, is_admin))

    else:
        id = insert_func(vals)

    if get_row_func is None:
        select_sql = """SELECT * FROM user
                       WHERE user.id=?;"""
        rows = data.read_query(select_sql, (id,))
    else:
        rows = get_row_func(id)

    return DBuser.create_dbuser(*rows[0]) if rows else None
