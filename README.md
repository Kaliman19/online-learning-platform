# Learning_platform Poodle

###### Developers:Alex Georgiev, Kalin Iliev

### OVERVIEW OF THE PROJECT

The design and implementation an e-learning platform that would be used by students to search for and enroll in online courses

and teachers who would publish the courses.It provides RESTful API that can be consumed by different clients.

The business logic has been encapsulated as much as possible in a separate layers. 

The chosen backend solution uses the language Python and the Restful framework - **_FastAPI_** in combination with 

relational database - MariaDB. For the simulation purposes of the real client-server interaction (integrational

tests - predominantly by hand) has been used POSTMAN. The business logic has been covered with unittesting. 

The E-learning platform consists of at least User, Courses, Sections, Review, and Tags. 

High-level description:

- Students can view available courses and access their content depending on whether the courses are public or premium and/or they have subscribed for them or not.

- Teachers can create and update courses, add, edit, and remove sections for them.

cd existing_repo
git remote add origin https://gitlab.com/Alexich/learning_platform-poodle.git
git branch -M main
git push -uf origin main

The application supports the following operations:

1. Anonymous users can register, log in and log out.They can view the title and description, tags of available public courses,
but not be able to open them.

2.  Students can view and edit their account information (except the username).

3. Students can view the courses that they are enrolled in (both public and premium).

4. Students can view and search through by name and tag existing public and premium courses.

5. Students can unsubscribe from premium courses.

6. Students can rate a course (only one score for a course) only if they are enrolled in it.

7. Students can subscribe to a maximum of 5 premium courses at a time and unlimited number of public courses.

8. Teachers can view and edit their account information (except the user name).

9. Teachers can create courses, view and update their own courses.

10. Teachers can approve enrollment requests sent by students.

