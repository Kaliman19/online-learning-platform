import unittest
from app.services import courses_services
from unittest import mock
from unittest.mock import  patch
from data.models import PublicCoursesInfo


class Courses_services_should_test(unittest.TestCase):

    def test_get_public_coursesReturns_Info(self):
        with patch("app.services.courses_services.read_query") as mock_db:
            mock_db.return_value=[("course1","descr1","some tag"),("course1","descr1","some tag")]
            result = list(courses_services.get_public_courses())
            self.assertEqual(2, len(result))
            self.assertIsInstance(result[0], PublicCoursesInfo)

