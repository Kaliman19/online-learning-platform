import unittest
from unittest.mock import Mock, patch
from app.services import teachers_services
from data.models import TeacherAccInfoResponse, TeacherAccInfoChange, CreateCourse,UpdateCourse,DBSection


class Teacher_Services_Should_test(unittest.TestCase):
    def setUp(self) -> None:
        self.stud_params = [
            (
                1,
                "Some username",
                "some firstname",
                "some lastname",
                "0987654",
                "some account link"
            )
        ]


        self.section_params = [
            (
                1,
                "Some title",
                "1",
                "some content"
            )
        ]

    # def tearDown(self) -> None:
    #     for k, v in vars(self).items():
    #         setattr(self, k, None)

    def test_view_acc_info_with_correctId(self):
        with patch("app.services.teachers_services.read_query") as mock_db:
            mock_db.return_value = self.stud_params

            result = teachers_services.view_acc_info(1)
            expected = TeacherAccInfoResponse.from_query_result(*self.stud_params[0])

            self.assertEqual(result, expected)

    def test_view_acc_infoReturns_None(self):
        with patch("app.services.teachers_services.read_query") as mock_db:
            mock_db.return_value = []

            result = teachers_services.view_acc_info(1)
            expected = None

            self.assertEqual(result, expected)

    def test_update_returnsUpdate_acc(self):
        with patch('app.services.teachers_services.update_query') as update_func:
            # Arrange
            update_func.return_value = True
            id = 1
            new_info = TeacherAccInfoChange(fname='Pesho', lname='Peshov',phone_num="888797",linked_acc="another one")
            result = teachers_services.update_acc(id, new_info)
            self.assertEqual(result, True)

    def test_create_course_returns_newID(self):
        with patch('app.services.teachers_services.insert_query') as insert_func:
            insert_func.return_value=1
            username = "fake one"
            course=CreateCourse(title= "fake title",
                                description= "fake descr",
                                objective="fake objective",
                                owner="fake owner",
                                is_premium=False,
                                is_deactivated=False,
                                home_page="fake page")
            result=teachers_services.create_course(course,username)
            self.assertEqual(result, 1)


    # def test_view_course_returns_course(self): #Not Working Yet
    #     with patch('app.services.teachers_services.read_query') as mock_db:
    #         id=1
    #         section=[("titl",1,"cont")]
    #         mock_db.return_value = [(1,'Python','This is good course','some objectives','an owner',False,False,'a page')]
    #         section = [("titl", 1, "cont")]
    #         result = list(teachers_services.view_course(id))
    #         self.assertEqual(1, len(result))
            # self.assertIsInstance(result[0], Course)

    # def test_find_section_Returns_listOf_sections(self):
    #     with patch("app.services.teachers_services.read_query") as mock_db:
    #         id=5
    #         mock_db.return_value = [(1,'title1',1, 'content1'),(2,'title2',2, 'content2')]
    #     result=teachers_services.find_sections(id)
    #     expected=[('title1',1, 'content1'),('title2',2, 'content2')]
    #     mock_db.return_value = self.stud_params
    #
    #     result = teachers_services.view_acc_info(1)
    #     expected = TeacherAccInfoResponse.from_query_result(*self.stud_params[0])
    #
    #     self.assertEqual(result, expected)

    def test_add_section_to_DB_returns_id(self):
         with patch("app.services.teachers_services.insert_query") as mock_db:
            mock_db.return_value=1
            self.assertIsInstance(mock_db.return_value,int)


    def test_update_courseReturns_true_if_updated(self):
        with patch('app.services.teachers_services.update_query') as update_func:
            # Arrange
            update_func.return_value = True
            id=1
            new=UpdateCourse(title='fake_t',
                             description='fake-descr',
                             objective="fake_obj",
                             is_premium=False,
                             is_deactivated=False,
                             home_page="home_p")
            result=teachers_services.update_course(id,new)
            self.assertEqual(result, True)

    def test_get_section_byId_returns_section(self):
        with patch("app.services.teachers_services.read_query") as mock_db:
            course_id=1
            section_id=2
            mock_db.return_value = self.section_params

            result = teachers_services.get_section_by_id(course_id,section_id)
            expected = DBSection.from_query_result(*self.section_params[0])

            self.assertEqual(result, expected)

    def test_get_Allsection_byId_returns_sections(self):
        with patch("app.services.teachers_services.read_query") as mock_db:
            course_id=1
            mock_db.return_value = [(1,'title1',1, 'content1'),(2,'title2',2, 'content2')]

            result = teachers_services.get_all_section(course_id)
            # expected = DBSection.from_query_result(*self.section_params[0])
            self.assertEqual(2, len(result))

    @patch('app.services.teachers_services.update_query', autospec=True)
    def test_remove_section_by_id(self, mock_update_query):
        result=teachers_services.remove_section_by_id(1,10)
        self.assertTrue(result)
    #
    @patch('app.services.teachers_services.read_query', autospec=True)
    def test_get_enrolled_students_returns_None(self, mock_db):
        mock_db.return_value=[]
        result=teachers_services.get_enrolled_students(1)
        expected=None
        self.assertEqual(result,expected)

    @patch('app.services.teachers_services.read_query', autospec=True)
    def test_get_enrolled_students_returns_correct_list(self, mock_db):
        mock_db.return_value = [(1,"username","first","second")]
        result = teachers_services.get_enrolled_students(1)
        self.assertEqual(len(result), 1)

    # @patch('app.services.teachers_services.update_query', autospec=True)
    # def test_approve_student_returnsCorrect_data(self,update_mock):
    #     update_mock.return_value = [("3","1","3")]
    #     result=teachers_services.approve_students(course=)
